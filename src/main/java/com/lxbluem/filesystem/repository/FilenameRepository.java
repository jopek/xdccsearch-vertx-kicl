package com.lxbluem.filesystem.repository;

import com.lxbluem.filesystem.FileEntity;

public interface FilenameRepository extends SimpleCrudRepository<FileEntity, FileEntity> {
}
